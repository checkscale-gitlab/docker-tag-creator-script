#!/bin/bash

set -e

#   ./buildDockerImageGitLab.sh "$APP_FLAVOR" "$APP_VERSION" "$APP_SPECIAL_VERSIONS" "$APP_ISDEFAULT" "$APP_SPECIAL_TAGS" "$APP_SUFFIX" "$APP_DISTRO"
#   Official release:   ./buildDockerImageGitLab.sh "official" "1.2.3" "" "false" "latest" "" "official"
#   Alpine release:     ./buildDockerImageGitLab.sh "alpine" "1.2.3" "" "false" "" "" "alpine"
#   Debian release:     ./buildDockerImageGitLab.sh "debian" "1.2.3" "" "false" "" "" "debian"

###############################################################################
#   Environment variables to set:
#       APP_FLAVOR              :   Set to tag images for different variants like "official", "iot", whatever you want.
#       APP_VERSION             :   Set to the application's version information
#       APP_SPECIAL_VERSIONS    :   Add these comma-seperated version information to the list of app's version based tag list
#       APP_ISDEFAULT           :   Set to true, if you want to include default tags like the version information only.
#       APP_SPECIAL_TAGS        :   Add these comma-seperated tags like "latest","stable"
#       APP_SUFFIX              :   Add a suffix to each APP_FLAVOR based tag, but not to the default tags.
#       APP_DISTRO              :   Subfolder containing the Dockerfile
#
#
#       CI-variables            :   GitLab CI/CD variables
###############################################################################

###############################################################################
#   Build, tag and push the image.
###############################################################################
echo "-------------------------------------------------------------------------------"
echo "Parsing commandline parameters:"

export APP_FLAVOR=$1
export APP_VERSION=$2
export APP_SPECIAL_VERSIONS=$3
export APP_ISDEFAULT=$4
export APP_SPECIAL_TAGS=$5
export APP_SUFFIX=$6
export APP_DISTRO=$7

if [ -z "$BUILD_DATE" ]; then
    export BUILD_DATE=$(date +"%Y.%m.%d %H:%M:%S")
    echo " - Build date (set): $BUILD_DATE"
fi

echo " - Flavor:           $APP_FLAVOR"
echo " - Version:          $APP_VERSION"
echo " - Special versions: $APP_SPECIAL_VERSIONS"
echo " - Is default:       $APP_ISDEFAULT"
echo " - Special tags:     $APP_SPECIAL_TAGS"
echo " - Tag suffix:       $APP_SUFFIX"
echo " - Subfolder:        $APP_DISTRO"
echo ""

echo "Generating tags run createTagList.sh:"
eval "$( ./createTagList.sh "$APP_FLAVOR" "$APP_VERSION" "$APP_SPECIAL_VERSIONS" "$APP_ISDEFAULT" "$APP_SPECIAL_TAGS" "$APP_SUFFIX" "$APP_DISTRO" )"

echo " - $TAG_LIST"
echo ""

echo "Build and tag the image:"

tags=( ${TAG_LIST//,/ } )
BUILT_TAG=""

for tag in "${tags[@]}"; do
    if [ "${BUILT_TAG}" = "" ]; then
        # Build the Docker image with tag "$CI_REGISTRY_IMAGE:${BUILT_TAG}"
        BUILT_TAG=${tag}
        echo "- Build image $CI_REGISTRY_IMAGE:${BUILT_TAG} ..."

        if [ ${#APP_DISTRO} -lt 1 ]; then
            DOCKERFILE_PATH="./Dockerfile"
        else
            DOCKERFILE_PATH="./$APP_DISTRO/Dockerfile"
        fi

        docker build \
            --build-arg ARG_APP_VERSION=$APP_VERSION \
            --build-arg ARG_APP_CHANNEL=$CI_COMMIT_REF_SLUG \
            --build-arg ARG_APP_COMMIT=$CI_COMMIT_SHA \
            --build-arg ARG_BUILD_DATE="$BUILD_DATE" \
            -f "$DOCKERFILE_PATH" \
            -t "$CI_REGISTRY_IMAGE:${BUILT_TAG}" .
    else
        # Tag and push image for each tag in list
        echo "- Tagging image:"
        echo "  - $CI_REGISTRY_IMAGE:${tag} ..."
        docker tag "$CI_REGISTRY_IMAGE:${BUILT_TAG}" "$CI_REGISTRY_IMAGE:${tag}"
    fi
done

echo "Push image and tags:"
for tag in "${tags[@]}"; do
    echo "  - $CI_REGISTRY_IMAGE:${tag} ..."
    docker push "$CI_REGISTRY_IMAGE:${tag}"
done

echo "-------------------------------------------------------------------------------"
echo "Building and tagging sequence:"
for tag in "${tags[@]}"; do
    echo "  - $CI_REGISTRY_IMAGE:${tag} "
done

echo "-------------------------------------------------------------------------------"
echo "Build succeeded!"
