#!/bin/bash

set -e

###############################################################################
#   Environment variables to set:
#       APP_FLAVOR              :   Set to tag images for different variants like "official", "iot", whatever you want.
#       APP_VERSION             :   Set to the application's version information
#       APP_SPECIAL_VERSIONS    :   Add these comma-seperated version information to the list of app's version based tag list
#       APP_ISDEFAULT           :   Set to true, if you want to include default tags like the version information only.
#       APP_SPECIAL_TAGS        :   Add these comma-seperated tags like "latest","stable"
#       APP_SUFFIX              :   Add a suffix to each APP_FLAVOR based tag, but not to the default tags.
#       APP_DISTRO              :   Not relevant for test patterns.
###############################################################################

###############################################################################
#   Create tag list for predefined contributing factors
###############################################################################
echo "-------------------------------------------------------------------------------"
echo "Generating tag lists ... "

rm -f test_*.txt

./createTagList.sh "" "1.2.3" "" "false" "" "" "" > ./testing/test_001.txt
./createTagList.sh "" "1.2.3" "" "true" "" "" "" > ./testing/test_002.txt
./createTagList.sh "" "-1.2.3" "" "false" "" "" "" > ./testing/test_003.txt
./createTagList.sh "" "-1.2.3" "" "true" "" "" "" > ./testing/test_004.txt
./createTagList.sh "flavor" "1.2.3" "" "false" "" "" "" > ./testing/test_005.txt
./createTagList.sh "flavor" "1.2.3" "" "true" "" "" "" > ./testing/test_006.txt
./createTagList.sh "flavor" "1.2.3" "" "false" "" "suffix" "" > ./testing/test_007.txt
./createTagList.sh "flavor" "1.2.3" "" "true" "" "suffix" "" > ./testing/test_008.txt
./createTagList.sh "" "1.2.3" "" "false" "" "suffix" "" > ./testing/test_009.txt
./createTagList.sh "" "1.2.3" "" "true" "" "suffix" "" > ./testing/test_010.txt
./createTagList.sh "" "1.2.3" "prod" "false" "" "" "" > ./testing/test_011.txt
./createTagList.sh "" "1.2.3" "prod" "true" "" "" "" > ./testing/test_012.txt
./createTagList.sh "flavor" "1.2.3" "prod" "false" "" "suffix" "" > ./testing/test_013.txt
./createTagList.sh "flavor" "1.2.3" "prod" "true" "" "suffix" "" > ./testing/test_014.txt
./createTagList.sh "" "1.2.3" "" "false" "latest" "" "" > ./testing/test_015.txt
./createTagList.sh "" "1.2.3" "" "true" "latest" "" "" > ./testing/test_016.txt
./createTagList.sh "" "1.2.3" "prod" "false" "latest" "" "" > ./testing/test_017.txt
./createTagList.sh "" "1.2.3" "prod" "true" "latest" "" "" > ./testing/test_018.txt
./createTagList.sh "flavor" "1.2.3" "prod" "false" "latest" "suffix" "" > ./testing/test_019.txt
./createTagList.sh "flavor" "1.2.3" "prod" "true" "latest" "suffix" "" > ./testing/test_020.txt

echo ""
echo "sha256 fingerprints ... "
sha256sum ./testing/test_*.txt

echo "-------------------------------------------------------------------------------"
